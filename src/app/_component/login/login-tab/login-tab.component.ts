import { Component, OnInit } from '@angular/core';
import { AuthenticationService} from '../../../_service/authentication.service';


@Component({
  selector: 'app-login-tab',
  templateUrl: './login-tab.component.html',
  styleUrls: ['../login.component.css']
})
export class LoginTabComponent implements OnInit {

  constructor(private auth:AuthenticationService) { }

  ngOnInit() {
  }


  login(){
  	this.auth.loginUser("any", "password").subscribe(data => {
        console.log(data);
    });
  }

  

}
