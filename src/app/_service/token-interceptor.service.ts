import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';

export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   
    if (request.headers.has(InterceptorSkipHeader)) {
      const headers = request.headers.delete(InterceptorSkipHeader);
      return next.handle(request.clone({headers}));
    } else {
      const token = localStorage.getItem('ctplToken');
      let ctpltoken = '';
      if (token) {
        ctpltoken = `bearer ${token}`;
      }

      request = request.clone({
        setHeaders: {
          Authorization: ctpltoken
        }
      });
      return next.handle(request);
    }
  }

}
