import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';

import {TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD,AUTH_TOKEN} from '../_constant/app.constant';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class AuthenticationService {

  constructor(private http:HttpClient) { }

  loginUser(username: string, password: string) {
    // console.log("Logging in User : " + username);
    const as = this;
    const body = `username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}&grant_type=password`;

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    headers = headers.set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));
    headers = headers.set(InterceptorSkipHeader, '');
    console.log(headers);
    return this.http.post(AUTH_TOKEN, body, {headers})
      .map((res: any) => {
        if (res) {
          return res;
        }
      }).catch((err: any) => {
        console.log('An error occurred:', err.error);
        return err.error;
      });
  }

}
