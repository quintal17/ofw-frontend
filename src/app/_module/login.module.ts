import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../_component/login/login.component';
import { LoginTabComponent } from '../_component/login/login-tab/login-tab.component'; 
import { AuthenticationService } from '../_service/authentication.service';
import { ForgotPasswordTabComponent } from '../_component/login/forgot-password-tab/forgot-password-tab.component';
import { AppMaterialModule } from './material.module';
         
@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule
  ],
  declarations: [
    LoginComponent,
    LoginTabComponent,
    ForgotPasswordTabComponent
  ],
  exports: [
    LoginComponent,
    LoginTabComponent,
    ForgotPasswordTabComponent
  ],
  providers:[
    AuthenticationService
  ]
})
export class LoginModule { }