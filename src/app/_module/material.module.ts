import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule, 
         MatButtonModule,
         MatCardModule,
         MatToolbarModule,
         MatTabsModule,
         MatFormFieldModule,
         MatGridListModule } from '@angular/material';
         
@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatTabsModule,
    MatFormFieldModule,
    MatGridListModule
  ],
  exports: [
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatTabsModule,
    MatFormFieldModule,
    MatGridListModule
  ]
})
export class AppMaterialModule { }